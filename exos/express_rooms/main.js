const reservations = require("./mock");
const express = require("express");

const app = express();

app.get("/rooms/", function(req,res) {
    res.json( {
	rooms: reservations.allRooms()
    });
});

//Completer les autres routes de l'exercice 7 en utilisant
//le module 'mock.js'

app.listen(8080);

