"use strict";

const fs = require("fs");
const http = require("http");

http.createServer(function(request, response) {
    fs.readFile("auto.js", "utf8", function(err, data) {
	if(err !== null ) {
	    response.writeHead(500, {"Content-Type": "text/plain"});
	    response.write("GENERAL INTERNAL FAILURE ERROR.");
	    response.end();
	    return;
	}
	response.writeHead(200, {"Content-Type": "text/javascript"});
	response.write(data);
	response.end();
    });
}).listen(8080);

