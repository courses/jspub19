echo "POST CORRECT"
echo
 curl -i  -H "Content-type: application/json; charset=UTF-8" -X POST -d '{"booker": "mxh", "from": "2018-01-01", "to": "2018-01-02"}' http://localhost:8080/room/r01/reservations
echo

echo
echo "POST Conflict"
echo
 curl -i  -H "Content-type: application/json; charset=UTF-8" -X POST -d '{"booker": "mxh", "from": "2018-01-01", "to": "2018-01-02"}' http://localhost:8080/room/r03/reservations
echo

echo
echo "POST Bad date"
echo
curl -i  -H "Content-type: application/json; charset=UTF-8" -X POST -d '{"booker": "mxh", "from": "2018-02-01", "to": "2018-01-02"}' http://localhost:8080/room/r01/reservations
echo


