"use strict";

const rooms = ["r01", "r02", "r03", "r04", "r05", "r06", "r07"];
const users = ["mxh", "nhu", "atl", "ffg", "pae", "jfe", "jct"];

function validInterval( from, to ) {
    return from.getTime() < to.getTime();
}


//List all existing rooms
function allRooms() {
    return rooms;
}

//List all free rooms for an interval
//Returns 'undefined' if the interval is not valid
function availableRooms( from, to ) {
    if( ! validInterval(from, to) ) {
	return undefined;
    } else {
	return ["r03","r07"];
    }
}

//List users with a reservation on a given room
//Returns undefined if the room does not exist
function roomBookers( roomID ) {
    if( ! rooms.includes( roomID ) ) {
	return undefined;
    } else {
	return [ "mxh", "nhu", "atl" ];
    }
}

//Attempt booking a room. In case of errors, the
//result has a field 'error' indicating the cause.
function bookAttempt( roomID, userID, from, to ) {
    if( ! validInterval( from, to ) ) {
	return { error: "date" };
    } else if( ! rooms.includes(roomID) ) {
	return { error: "roomID" };
    } else if( roomID === "r03" ) {
	return {
	    error: "conflict",
	    reservation: {
		ID: 1102,
		booker: "mxh",
		from: from,
		to: to
	    }
	};
    } else {
	return {
	    success: {
		ID: 1103,
		booker: userID,
		from: from,
		to: to
	    }
	};
    }
}

//List all reservation of an user. Returns undefined
//if the user does not exist.
function reservationsOf( userID ) {
    if( ! users.includes(userID ) ) {
	return undefined;
    }
    return [{
	ID: 1100,
	booker: userID,
	room: "r04",
	from: new Date("2015-03-25T11:00:00Z"),
	to: new Date("2015-03-25T13:00:00Z")
    }, {
	ID: 1101,
	booker: userID,
	room: "r04",
	from: new Date("2015-03-26T9:15:00Z"),
	to: new Date("2015-03-26T17:30:00Z")
    }];
}

//Delete a reservation, returns true iff the res. did exist
function deleteReservation( reservationID ) {
    return reservationID === 1102;
}

//Atempt to modify a reservation, in case of error, the
//result has a field error with the issue.
function modifyReservation( reservationID, roomID, userID, from, to ) {
    let res = bookAttempt( roomID, userID, from, to );
    if( res.error === undefined ) {
	res.room = roomID;
    } 
    return res;
}

exports.allRooms = allRooms;
exports.availableRooms = availableRooms;
exports.roomBookers = roomBookers;
exports.reservationAttempt = bookAttempt;
exports.reservationsOf = reservationsOf;
exports.deleteReservation = deleteReservation;
exports.modifyReservation = modifyReservation;
