"use strict";

const reservations = require("./mock");
const err = require("./errors");
const express = require("express");

const app = express();

app.use(express.json());

function parseDate( str ) {
    let d = new Date( str );
    if( isNaN( d.getTime() ) ) {
	return undefined;
    } else {
	return d;
    }
}

app.get("/rooms/",
	function(req,res) {
	    if( req.query.status === undefined ) {
		res.json( {
		    rooms: reservations.allRooms()
		});
		return;
	    }
	    if( req.query.status === "free" ) {
		let from = parseDate(req.query.from);
		let to = parseDate(req.query.to);
	    if( from === undefined || to === undefined ) {
		err.badDates(res);
		return;
	    }
		let rooms = reservations.availableRooms(from,to);
		if( rooms === undefined ) {
		    err.badDates(res);
		    return;
		}
		res.json( { rooms: rooms } );
	    }
	}
       );


app.get("/room/:roomID/bookers", function( req, res ) {
    let roomID = req.params.roomID;
    let bookers = reservations.roomBookers( roomID );
    if( bookers === undefined ) {
	err.badRoom(res,roomID);
	return;
    }
    res.json( { bookers: bookers } );
});


function processResult( res, result ) {
    if( result.error !== undefined ) {
	if( result.error === "date" ) {
	    err.badDates(res);
	    return;
	}
	if( result.error === "roomID" ) {
	    err.badRoom(res,roomID);
	    return;
	}
	if( result.error === "conflict" ) {
	    err.conflict(res,result.reservation);
	    return;
	}
    }
    res.json( result.success );
}


app.post("/room/:roomID/reservations", function(req,res) {
    let roomID = req.params.roomID;
    let reservation = req.body;
    let userID = reservation.booker;
    let from = parseDate(reservation.from);
    let to = parseDate(reservation.to);
    if( from === undefined || to === undefined ) {
	err.badDates(res);
	return;
    }
    let result = reservations.reservationAttempt(roomID,userID,from,to);
    processResult( res, result );
});

app.get("/users/:userID/reservations", function(req,res) {
    let userID = req.params.userID;
    let result = reservations.reservationsOf(userID);
    if( result === undefined ) {
	err.badUser( res, userID );
	return;
    }
    res.json( result );
});

app.get("/reservations", function(req,res) {
    let userID = req.query.booker;
    if( userID !== undefined ) {
	res.redirect( "/users/" +userID+ "/reservations");
    }
});

app.delete("/reservation/:resID", function(req,res) {
    reservations.deleteReservation(req.params.resID);
    res.status(200).send("Deleted");
});

app.put("/reservation/:resID", function(req,res) {
    let resID = req.params.resID;
    let reservation = req.body;
    let userID = reservation.booker;
    let roomID = reservation.room;
    let from = parseDate(reservation.from);
    let to = parseDate(reservation.to);
    if( from === undefined || to === undefined ) {
	err.badDates(res);
	return;
    }
    let result = reservations.reservationAttempt(roomID,userID,from,to);
    processResult( res, result );
});




app.listen(8080);

