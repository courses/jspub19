"use strict";

function error( res, code, msg ) {
    res.status(code)
	.set( "Content-Type", "text/plain" )
	.send( msg );
}

exports.badDates = function(res) {
    error( res, 400, "Bad date format." );
};
exports.badRoom = function(res,roomID) {
    error( res, 404, "No room with ID: " + roomID );
};
exports.badUser = function(res,userID) {
    error( res, 404, "No user with ID: " + userID );
};
exports.conflict = function(res,reservation) {
    res.status(409).json(reservation);
};

