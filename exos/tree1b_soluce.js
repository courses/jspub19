/* Retourne un arbre contenant une valeur unique */
function single( x ) {
    return { value: x, children: [] };
}

/* Retourne un nouvel arbre issu de la fusion des deux arbres.
   Ceux-ci ne sont pas modifiés */
function join( tree1, tree2 ) {
    let children2 = Array.from(tree1.children);
    children2.push( tree2 );
    return { value: tree1.value, children: children2 };
}

/* Ajoute une valeur à la droite d'un arbre, celui-ci n'est pas modifié */
function append( tree, x ) {
    return join( tree, single(x) );
}

/* Ajoute une valeur à la gauche d'un arbre, celui-ce n'est modifié */
function prepend( tree, x ) {
    return join( single(x), tree );
}

/* Retourne la taille d'un arbre (nombre de valeurs dans l'arbre) */
function size( tree ) {
    let result = 1;
    let i = 0;
    while( i < tree.length ) {
	let next = tree.children[i];
	result += size(next);
	i += 1;
    }
    return result;
}

/* Retourne la longueur de la branche la plus profonde d'un arbre */
function depth( tree ) {
    let result = 1;
    let i = 0;
    while( i < tree.length ) {
	let next = tree.children[i];
	result = Math.max( result, depth(next) );
	i += 1;
    }
    return result;
}


/* Retourne un tableau comprenant toutes les valeurs de l'arbre
   ordonnées selon un parcours en profondeur */
function toArray( tree ) {
    let result = [ tree.value ];
    let i = 0;
    while( i < tree.children.length ) {
	let next = tree.children[i];
	result = result.concat( toArray(next) );
	i += 1;
    }
    return result;
}
