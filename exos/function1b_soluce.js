/* Retourne un arbre contenant une valeur unique */
function single( x ) {
  return { value: x, children: [] };
}

/* Retourne un nouvel arbre issu de la fusion des deux arbres.
   Ceux-ci ne sont pas modifiés */
function join( tree1, tree2 ) {
  let children2 = Array.from( tree1.children );
  children2.push( tree2 );
  return { value: tree1.value, children: children2 };
}

/* Ajoute une valeur à la droite d'un arbre, celui-ci n'est pas modifié */
function append( tree, x ) { 
  return join( tree, single(x) );
}

/* Ajoute une valeur à la gauche d'un arbre, celui-ce n'est modifié */
function prepend( tree, x ) { 
  return join( single(x), tree );
}

/* Retourne la taille d'un arbre (nombre de valeurs dans l'arbre) */
function size( tree ) {
  return tree.children.map( size ).reduce( (x,y) => x+y, 0 ) + 1;
}

/* Retourne la longueur de la branche la plus profonde d'un arbre */
function depth( tree ) {
  return tree.children.map( depth ).reduce( Math.max, 0 ) + 1;
}

/* Retourne un tableau comprenant toutes les valeurs de l'arbre
   ordonnées selon un parcours en profondeur */
function toArray( tree ) { 
   return tree.children
      .map( toArray )
      .reduce( (x,y) => x.concat(y), [tree.value] );
   /*let childrenArray = reduce( (x,y) => x.concat(y), [] ); 
   return [tree.value].concat( childrenArray );  */ 
}

function fixSnd( f, a ) {
	return (x) => f(x,a);
}

function map( tree, f ) {
  return { 
    value: f(tree.value),
    children: tree.children.map( (child) => map(child,f) )
  };
}

function reduce( tree, h ) {
  return tree.children
    .map( (child) => reduce(child, h ) )
    .reduce( h, tree.value );
}
   
function plus(x,y) {
  return x+y;
}
   
function sum( tree ) {
  return reduce( tree, plus );
}

function avgStr( tree ) {
  return sum( map( tree, (x) => x.length ) ) / size(tree);
  //return sum(tree).length / size(tree);
}
   
  
