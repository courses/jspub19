"use strict";

const Tree = function() {

    function Node( value, children ) {
	this.value = value;
	this.children = children;
    }

    function single(x) {
	    return new Node(x, [] );
    }

    
    function size( tree ) {
	return tree.children.map( size ).reduce( (x,y) => x+y, 0 ) + 1;
    }

    function toArray( tree ) { 
	return tree.children
	    .map( toArray )
	    .reduce( (x,y) => x.concat(y), [tree.value] );
    }

    function reduce( tree, h ) {
	return tree.children
	    .map( (child) => reduce(child, h ) )
	    .reduce( h, tree.value );
    }

    function map( tree, f ) {
	return new Node( 
	    f(tree.value),
	    tree.children.map( (child) => map(child,f) )
	);
    }

    
    Node.prototype = {
	size: function() {
	    return size(this);
	},
	prepend: function(x) {
	    return single(x).join( this );
	},
	append: function(x) {
	    return this.join( single(x) );
	},
	join: function(that) {
	    let children2 = Array.from(this.children);
	    children2.push(that);
	    return new Node(
		this.value,
		children2
	    );
	},
	toArray: function() {
	    return toArray(this);
	},
	map: function(f) {
	    return map(this,f);
	},
	reduce: function(f) {
	    return reduce(this,f);
	},
	
    }
    
    return {
	single: single
    };
}();


let tree0 = Tree.single("A"); // Arbre vide

console.log( tree0.size() );  //DOIT afficher 1

let tree1 = tree0.append("B").append("C").append("D");

console.log( tree1.toArray() );  //DOIT afficher ["A","B","C","D"]



let tree2 = Tree.single(102).prepend(101).prepend(100);

let tree3 = tree1.join( tree2 ); 

console.log( tree3.size() );  //DOIT afficher 7
console.log( tree3.toArray() );  //DOIT afficher ["A","B","C","D",100,101,102]

console.log( tree3 );

let total = tree2.map( (x) => x - 100 ).reduce( (x,y) => x+y );
console.log( total ); //DOIT affiche 3

